# techdoc-prototype
## Pre-installation
You need to install NodeJS (LTS version, today is 10.16.0) from official
[site](https://nodejs.org/uk/).
## Installation
1. Install this repository with command: 
    ```
    git clone  https://DarthSpeedius@bitbucket.org/DarthSpeedius/techdoc-prototype.git
    ```
2. Go to directory and install npm packages: 
    ```
    cd techdoc-prototype
    npm install
    ```
3. **(Not neccesary)** For development, you need install nodemon and
   typescript:
    ```
    npm install -g nodemon tsc typescript
    ```
## Run server
#### Production
```shell
npm run production
```
#### Development
```shell
npm start
```
## Available endpoints: 
* **POST --- http://localhost:3000/orders** – create new orders(fake) and send email to client with generated pdf.

  params (name: **string**, phone: **string**, email: **string**) 