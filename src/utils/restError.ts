
export default class RestError extends Error{
    public status?: number
    constructor(message: string, status:number = 500){
        super(message);
        this.message = message;
        this.status = status;
    }
}