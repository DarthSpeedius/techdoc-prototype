import observer from "./observer";

export default class Logger{
    constructor(){
        observer.on("log", (data) => {
            console.log(data);
        });
    }
};