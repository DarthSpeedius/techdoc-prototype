import observer from "./observer";
import {Request, Response} from "express";

export default (req: Request, res: Response, next) => {
    const sendResponse = Symbol(`sendResponse`);
    res.locals.sendResponseEvent = sendResponse;
    let isSent = false;
    const apiSender = (data) => {
        if(res.finished) return;
        res.json({
            success: true,
            data
        });
        isSent = true;
    };
    observer.emit("log", `${req.method} --- ${req.baseUrl}`);
    observer.once(sendResponse, apiSender);
    observer.once("error", (err) => {
        if(!isSent){
            res.json({
                success: false,
                status: err.status || 500,
                message: err.message || "Unknown server error"
            });
            isSent = true;
        }
    })
    return next();
}