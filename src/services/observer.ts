const EventEmitter = require('events');
let observer = null;

class Observer extends EventEmitter{
    asyncEvents = {};

    async emitAsync(eventName: string, data: any){
        const promise = this.asyncEvents[eventName];
        return promise(data);
    }

    onAsync(eventName: string, promise){
        this.asyncEvents[eventName] = promise;
    }

    offAsync(eventName: string){
        delete this.asyncEvents[eventName];
    }
}

if(!observer) {
    observer = new Observer();
}

export default observer;