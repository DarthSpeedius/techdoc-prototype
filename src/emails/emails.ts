import * as fs from "fs";
import * as path from "path";
import * as ejs from "ejs";
import * as nodemailer from "nodemailer";
import * as config from "../../config";
import observer from "../services/observer";

const newOrdersTemplate = fs.readFileSync(path.resolve(__dirname, `../../templates/newOrder.ejs`)).toString();

export const EMAIL_TYPES = {
    NEW_ORDER: newOrdersTemplate
}

export default class Emails{
    constructor() {
        observer.on("emails:send", this.send);
    }

    send = ({to, data, template, attachments = []}) => {
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: config.email.username,
                pass: config.email.password
            }
        });
        const html = ejs.render(template, data);
        const mailOption = {
            from: config.email.username,
            to,
            subject: `﻿New order №${data.orderId}`,
            html,
            attachments
        };
        return transporter.sendMail(mailOption);
    }
}