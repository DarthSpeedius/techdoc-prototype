import {Request, Response, Router} from "express";
import observer from "../services/observer";
import EndpointInterface from "../types/endpointInterface";
import {EMAIL_TYPES} from "../emails/emails";
import {PDF_TEMPLATES} from "../pdfs/pdfs";
import RestError from "../utils/restError";

export default class Orders implements EndpointInterface{
    constructor() {
        this.router.post("/", this.createOrderValidate, this.createOrder);
    }

    createOrderValidate(req: Request, res: Response, next){
        try {
            if (!(req.body.name)) {
                throw new RestError("Please fill name", 400);
            }
            if (!(req.body.email)) {
                throw new RestError("Please fill email", 400);
            }
            if (!(req.body.phone)) {
                throw new RestError("Please fill phone", 400);
            }
            return next();
        } catch(err){
            return next(err);
        }
    }

    async createOrder(req: Request, res: Response){
        try {
            const id = 1496;
            observer.emit(res.locals.sendResponseEvent, {
                id,
                time: new Date()
            });

            const pdfPath = await observer.emitAsync("pdf:generate", {
                template: PDF_TEMPLATES.NEW_ORDER,
                data: {
                    name: req.body.name,
                    orderId: id
                }
            });

            observer.emit("emails:send", {
                data: {
                    name: req.body.name,
                    orderId: id
                },
                to: [req.body.email],
                attachments: [{
                    encoding: "utf-8",
                    path: pdfPath
                }],
                template: EMAIL_TYPES.NEW_ORDER
            });
        } catch(err){
            observer.emit("error", err);
        }
    }

    router = Router()

}