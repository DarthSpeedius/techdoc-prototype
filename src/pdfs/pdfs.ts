import * as fs from "fs";
import * as path from "path";
import * as Html5ToPdf from "html5-to-pdf";
import * as ejs from "ejs";

import observer from "../services/observer";


const newOrdersTemplate = fs.readFileSync(path.resolve(__dirname, `../../templates/newOrder.ejs`)).toString();


export const PDF_TEMPLATES = {
  NEW_ORDER: newOrdersTemplate
};

export default class PDFGenerator {
    constructor() {
        observer.onAsync("pdf:generate", this.generate);
        observer.on("pdf:generate", this.generate);
    }

    async generate({data, template}){
        const html = ejs.render(template, data);
        const absPath = path.resolve(__dirname, `../../generatedPDF/${data.name}-order.pdf`);
        const file = new Html5ToPdf({
            inputBody: html,
            outputPath: absPath
        });

        await file.start();
        await file.build();
        await file.close();
        return absPath;
    }
}