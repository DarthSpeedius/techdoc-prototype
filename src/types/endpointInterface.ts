import {Router} from "express";

export default interface EndpointInterface {
    router: Router
}