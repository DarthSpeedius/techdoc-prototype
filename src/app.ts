import * as express from "express";
import * as bodyParser from "body-parser";

import * as config from "../config";
import api from "./services/api";
import Logger from "./services/logger";
import Orders from "./orders/orders";
import Emails from "./emails/emails";
import PDFGenerator from "./pdfs/pdfs";
import RestError from "./utils/restError";

// init all modules
new Logger();
new Emails();
const orders = new Orders();
const pdf = new PDFGenerator();

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use("/orders/", api, orders.router);

app.use((req, res, next) => {
    const error = new RestError("Not found", 404);
    next(error);
})
app.use((err, req, res, next) => {
    res.json({
        success: false,
        message: err.message || "Unknown error",
        status: err.status || 500
    })
});

app.listen(config.port, () => {
    console.log(`App started at port ${config.port}`);
});